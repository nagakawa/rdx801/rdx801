pub mod abstract_shard;
pub mod evcmd;
mod fsm;

pub type Shard<'c, 'f, 's, E> = abstract_shard::Shard<'c, 'f, 's, E>;
pub type ShardFsm<'a, 's> = fsm::ShardFsm<'a, 's>;
pub type ShardEvent = evcmd::ShardEvent;
