pub mod abstract_database;
pub mod evcmd;
mod fsm;

pub type Database<'a, 's, E> = abstract_database::Database<'a, 's, E>;
pub type DatabaseFsm<'a, 's> = fsm::DatabaseFsm<'a, 's>;
pub type DbEvent = evcmd::DbEvent;
