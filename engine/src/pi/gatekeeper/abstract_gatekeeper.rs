use pi::configs::GatekeeperConfig;
use pi::gatekeeper::evcmd::{GatekeeperCommand, GatekeeperEventType};
use pi::gatekeeper::{GatekeeperEvent, GatekeeperFsm};

pub trait Gatekeeper<'c, 'f, 's : 'c, E> {
    fn new(config: &'c GatekeeperConfig<'s>, fsm: &'f mut GatekeeperFsm<'c, 's>) -> Self;
    fn get_fsm(&mut self) -> &mut GatekeeperFsm<'c, 's>;
    fn recv_response(&mut self, cmd: GatekeeperCommand) -> ();
    fn start(&mut self) -> Result<(), E>;
    fn send_event(&mut self, ev: GatekeeperEvent) -> () {
        self.get_fsm().recv(ev);
        let mut v: Vec<_> = self.get_fsm().outgoing.drain(..).collect();
        for cmd in v.drain(..) {
            self.recv_response(cmd);
        }
    }
}
