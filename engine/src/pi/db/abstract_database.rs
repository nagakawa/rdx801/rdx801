use pi::configs::DbConfig;
use pi::db::evcmd::{DbCommand, DbEventType};
use pi::db::{DatabaseFsm, DbEvent};

pub trait Database<'a, 's: 'a, E> {
    fn new(config: &'a DbConfig<'s>, fsm: &'a mut DatabaseFsm<'a, 's>) -> Self;
    fn get_fsm(&mut self) -> &mut DatabaseFsm<'a, 's>;
    fn recv_response(&mut self, cmd: DbCommand) -> ();
    fn start(&mut self) -> Result<(), E>;
    fn send_event(&mut self, ev: DbEvent) -> () {
        self.get_fsm().recv(ev);
        let mut v: Vec<_> = self.get_fsm().outgoing.drain(..).collect();
        for cmd in v.drain(..) {
            self.recv_response(cmd);
        }
    }
}
