use std::time::Instant;

use pi::packet::*;
use pi::rpc::RpcTrans;

#[derive(Serialize, Deserialize, Debug)]
pub enum ShardEventType {
    FromClient(RpcTrans, CsRequest),
    FromClientP(CsPacket),
    FromGatekeeperReq(RpcTrans, GsRequest),
    FromGatekeeperRes(RpcTrans, SgResponse),
    FromDb(RpcTrans, XdResponse),
    File(FileResponse),
    Tick,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ShardEvent {
    pub timestamp: Timestamp,
    pub content: ShardEventType,
}

#[derive(Serialize, Deserialize, Debug)]
pub enum ShardCommand {
    ToClient(RpcTrans, CsResponse),
    ToClientP(ScPacket),
    ToGatekeeperReq(RpcTrans, SgRequest),
    ToGatekeeperRes(RpcTrans, GsResponse),
    ToDb(RpcTrans, XdRequest),
    File(FileRequest),
}
