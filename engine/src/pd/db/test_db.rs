use pi::configs::DbConfig;
use pi::db::abstract_database::Database;
use pi::db::evcmd::{DbCommand, DbEventType};
use pi::db::{DatabaseFsm, DbEvent};

pub struct TestDb<'a, 's: 'a> {
    input: Vec<DbEvent>,
    pub output: Vec<DbCommand>,
    config: &'a DbConfig<'s>,
    fsm: &'a mut DatabaseFsm<'a, 's>,
}

impl<'a, 's> Database<'a, 's, ()> for TestDb<'a, 's> {
    fn new(config: &'a DbConfig<'s>, fsm: &'a mut DatabaseFsm<'a, 's>) -> Self {
        TestDb {
            input: vec![],
            output: vec![],
            config,
            fsm,
        }
    }
    fn get_fsm(&mut self) -> &mut DatabaseFsm<'a, 's> {
        self.fsm
    }
    fn recv_response(&mut self, cmd: DbCommand) {
        self.output.push(cmd);
    }
    fn start(&mut self) -> Result<(), ()> {
        // If anyone knows of a better way than this to appease the
        // compiler, then let me know.
        let mut v: Vec<_> = self.input.drain(..).collect();
        for ev in v.drain(..) {
            self.send_event(ev);
        }
        Ok(())
    }
}

impl<'a, 's> TestDb<'a, 's> {
    pub fn with_input(
        config: &'a DbConfig<'s>,
        fsm: &'a mut DatabaseFsm<'a, 's>,
        input: Vec<DbEvent>,
    ) -> Self {
        TestDb {
            input,
            output: vec![],
            config,
            fsm,
        }
    }
}
