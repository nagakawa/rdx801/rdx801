use pi::client::evcmd::{ClientCommand, ClientEventType};
use pi::client::{ClientEvent, ClientFsm};
use pi::configs::ClientConfig;

pub trait Client<'c, 'f, E> {
    fn new(config: &'c ClientConfig, fsm: &'f mut ClientFsm<'c>) -> Self;
    fn get_fsm(&mut self) -> &mut ClientFsm<'c>;
    fn recv_response(&mut self, cmd: ClientCommand) -> ();
    fn start(&mut self) -> Result<(), E>;
    fn send_event(&mut self, ev: ClientEvent) -> () {
        // Doing this synchronously.
        // Would it be a better idea to queue the events up
        // and process the queue once a frame?
        self.get_fsm().recv(ev);
        let mut v: Vec<_> = self.get_fsm().outgoing.drain(..).collect();
        for cmd in v.drain(..) {
            self.recv_response(cmd);
        }
    }
}
