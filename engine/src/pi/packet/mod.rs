use std::fmt;
use std::net::IpAddr;
use std::time::SystemTime;

use serde::de;
use serde::de::{Deserialize, Deserializer, SeqAccess, Visitor};
use serde::ser::{Serialize, Serializer};

pub const CLIENTSIDE_HASH_LEN: usize = 256;
pub const COOKIE_LEN: usize = 16;
pub type Cookie = [u8; COOKIE_LEN];
pub type PlayerId = u32;
pub type ShardId = u64;

// Fuck me all over if I have to do this shit ever again.

pub struct ClientHash {
    data: Box<[u8; CLIENTSIDE_HASH_LEN]>,
}

pub struct ClientHashVisitor {}

impl Serialize for ClientHash {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        use serde::ser::SerializeTuple;
        let mut seq = serializer.serialize_tuple(CLIENTSIDE_HASH_LEN)?;
        for i in 0..CLIENTSIDE_HASH_LEN {
            seq.serialize_element(&self.data[i])?;
        }
        seq.end()
    }
}

impl<'de> Visitor<'de> for ClientHashVisitor {
    type Value = ClientHash;
    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(
            formatter,
            "a byte array containing {} bytes",
            CLIENTSIDE_HASH_LEN
        )
    }

    #[inline]
    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
    where
        A: SeqAccess<'de>,
    {
        let mut a: [u8; CLIENTSIDE_HASH_LEN] = [0; CLIENTSIDE_HASH_LEN];
        for i in 0..256 {
            a[i] = match seq.next_element()? {
                Some(val) => val,
                None => return Err(de::Error::invalid_length(i, &self)),
            };
        }
        Ok(ClientHash { data: Box::new(a) })
    }
}

impl<'de> Deserialize<'de> for ClientHash {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_tuple(CLIENTSIDE_HASH_LEN, ClientHashVisitor {})
    }
}

impl fmt::Debug for ClientHash {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ClientHash {{ data: ");
        for i in 0..256 {
            write!(f, "{:x}", self.data[i]);
        }
        write!(f, " }}")
    }
}

impl PartialEq for ClientHash {
    fn eq(&self, other: &ClientHash) -> bool {
        // XXX: NOT CONSTANT TIME
        for i in 0..256 {
            if self.data[i] != other.data[i] {
                return false;
            }
        }
        true
    }
}

// serde doesn't implement the appropriate traits for
// Instant, so we use SystemTime for now and change to Instant
// when it's supported.
pub type Timestamp = SystemTime;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum LoginStatus {
    Ok(Cookie, PlayerId, IpAddr, u16),
    InvalidCredentials,
    ServerFull,
    Banned(String), // reason
    AlreadyLoggedIn,
    ServerDown(String), // reason
    UnknownFail,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum LoginStatusDb {
    Ok(PlayerId),
    InvalidCredentials,
    ServerFull,
    Banned(String), // reason
    AlreadyLoggedIn,
    ServerDown(String), // reason
    UnknownFail,
}

/*
    These types are named as such:

    * The prefix has two letters, each of which can be one of the
      following:
        * C: client
        * G: gatekeeper
        * S: shard
        * D: database
        * X: either gatekeeper or shard
    * RPC requests and responses end with `Request` and `Response`,
      respectively. The first letter of the prefix corresponds to
      the requester; the second corresponds to the responder.
    * Non-RPC packets end with `Packet`. First letter of prefix is
      the sender; the second letter is the receiver.
*/

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum CgRequest {
    Login(String, ClientHash),
    Motd,
    Filehost,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum CgResponse {
    Login(LoginStatus),
    Motd(String),
    Filehost(String),
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum CsRequest {
    //
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum CsResponse {
    //
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum CsPacket {
    //
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum ScPacket {
    //
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum SgPacket {
    // Sent from time to time by the shards to tell the gatekeeper how many
    // players are online vs. how many players the shard can hold.
    PlayerCount(u32, u32), // current, max
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum GsRequest {
    //
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum GsResponse {
    //
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum SgRequest {
    //
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum SgResponse {
    //
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum XdRequest {
    // Sent only by the gatekeeper.
    // Tell the database to accept connections from a shard
    // and give it its IP address and name.
    AddShard(IpAddr, ShardId),
    // Given the username and the client-hashed password, ask
    // the database if the user should be logged in
    Authenticate(String, ClientHash),
    // Look up the username for the player ID.
    Username(PlayerId),
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum XdResponse {
    AddShard(bool /* success */),
    Authenticate(LoginStatusDb),
    Username(String),
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct FileRequest {
    fname: String,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct FileResponse {
    fname: String,
    contents: Vec<u8>,
}
