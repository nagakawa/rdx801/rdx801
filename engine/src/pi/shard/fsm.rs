use std::collections::VecDeque;

use pi::configs::ShardConfig;
use pi::shard::evcmd::{ShardCommand, ShardEvent};

pub struct ShardFsm<'a, 's: 'a> {
    pub outgoing: VecDeque<ShardCommand>,
    config: &'a ShardConfig<'s>,
}

impl<'a, 's> ShardFsm<'a, 's> {
    pub fn new(config: &'a ShardConfig<'s>) -> Self {
        ShardFsm {
            outgoing: VecDeque::new(),
            config,
        }
    }
    pub fn recv(&mut self, ev: ShardEvent) -> () {
        // ???
    }
    pub fn send_cmd(&mut self, cmd: ShardCommand) -> () {
        self.outgoing.push_back(cmd);
    }
}
