use std::collections::VecDeque;

use pi::client::evcmd::{ClientCommand, ClientEvent};
use pi::configs::ClientConfig;

pub struct ClientFsm<'a> {
    pub outgoing: VecDeque<ClientCommand>,
    config: &'a ClientConfig,
}

impl<'a> ClientFsm<'a> {
    pub fn new(config: &'a ClientConfig) -> Self {
        ClientFsm {
            outgoing: VecDeque::new(),
            config,
        }
    }
    pub fn recv(&mut self, ev: ClientEvent) -> () {
        // ???
    }
    pub fn send_cmd(&mut self, cmd: ClientCommand) -> () {
        self.outgoing.push_back(cmd);
    }
}
