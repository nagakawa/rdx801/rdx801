use std::net::SocketAddr;

pub struct ClientConfig {
    //
}

#[derive(Clone)]
pub struct ServerLayout {
    pub gatekeeper: SocketAddr,
    pub database: SocketAddr,
    // Some(...) if shards are fixed; None if they are dynamic
    pub shards: Option<Vec<SocketAddr>>,
}

pub struct GatekeeperConfig<'a> {
    pub server_layout: &'a ServerLayout,
    pub motd: String,
    pub filehost_uri: String,
}

pub struct ShardConfig<'a> {
    pub server_layout: &'a ServerLayout,
    pub max_players: u32,
}

pub struct DbConfig<'a> {
    pub server_layout: &'a ServerLayout,
}
