use std::collections::{HashSet, VecDeque};

use pi::configs::GatekeeperConfig;
use pi::gatekeeper::evcmd::{GatekeeperCommand, GatekeeperEvent};
use pi::packet::*;

pub struct GatekeeperFsm<'a, 's: 'a> {
    pub outgoing: VecDeque<GatekeeperCommand>,
    config: &'a GatekeeperConfig<'s>,
    current_shards: HashSet<ShardId>,
}

impl<'a, 's> GatekeeperFsm<'a, 's> {
    pub fn new(config: &'a GatekeeperConfig<'s>) -> Self {
        GatekeeperFsm {
            outgoing: VecDeque::new(),
            config,
            current_shards: HashSet::<ShardId>::new(),
        }
    }
    pub fn recv(&mut self, ev: GatekeeperEvent) -> () {
        use pi::gatekeeper::evcmd::GatekeeperCommand::*;
        use pi::gatekeeper::evcmd::GatekeeperEventType::*;
        use pi::packet::{CgRequest, CgResponse};
        match ev.content {
            FromClient(txn, CgRequest::Motd) => {
                let motd = self.get_motd();
                self.send_cmd(ToClient(txn, CgResponse::Motd(motd)));
            }
            FromClient(txn, CgRequest::Filehost) => {
                let fhost = self.get_filehost_address();
                self.send_cmd(ToClient(txn, CgResponse::Filehost(fhost)));
            }
            // FromClient(txn, Login(un, ph)) => {}
            AddShard(sid) => {
                self.current_shards.insert(sid);
            }
            RemoveShard(sid) => {
                self.current_shards.remove(&sid);
            }
            _ => {
                eprintln!("Warn: no handler for {:?}", ev);
            }
        }
    }
    pub fn send_cmd(&mut self, cmd: GatekeeperCommand) -> () {
        self.outgoing.push_back(cmd);
    }
    pub fn get_motd(&self) -> String {
        self.config.motd.clone()
    }
    pub fn get_filehost_address(&self) -> String {
        self.config.filehost_uri.clone()
    }
    pub fn has_shard(&self, s: ShardId) -> bool {
        self.current_shards.contains(&s)
    }
}
