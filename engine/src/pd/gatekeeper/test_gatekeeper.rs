use pi::configs::GatekeeperConfig;
use pi::gatekeeper::abstract_gatekeeper::Gatekeeper;
use pi::gatekeeper::evcmd::{GatekeeperCommand, GatekeeperEventType};
use pi::gatekeeper::{GatekeeperEvent, GatekeeperFsm};

pub struct TestGatekeeper<'c, 'f, 's : 'c> {
    input: Vec<GatekeeperEvent>,
    pub output: Vec<GatekeeperCommand>,
    config: &'c GatekeeperConfig<'s>,
    fsm: &'f mut GatekeeperFsm<'c, 's>,
}

impl<'c, 'f, 's> Gatekeeper<'c, 'f, 's, ()> for TestGatekeeper<'c, 'f, 's> {
    fn new(config: &'c GatekeeperConfig<'s>, fsm: &'f mut GatekeeperFsm<'c, 's>) -> Self {
        TestGatekeeper {
            input: vec![],
            output: vec![],
            config,
            fsm,
        }
    }
    fn get_fsm(&mut self) -> &mut GatekeeperFsm<'c, 's> {
        self.fsm
    }
    fn recv_response(&mut self, cmd: GatekeeperCommand) {
        self.output.push(cmd);
    }
    fn start(&mut self) -> Result<(), ()> {
        // If anyone knows of a better way than this to appease the
        // compiler, then let me know.
        let mut v: Vec<_> = self.input.drain(..).collect();
        for ev in v.drain(..) {
            self.send_event(ev);
        }
        Ok(())
    }
}

impl<'c, 'f, 's> TestGatekeeper<'c, 'f, 's> {
    pub fn with_input(
        config: &'c GatekeeperConfig<'s>,
        fsm: &'f mut GatekeeperFsm<'c, 's>,
        input: Vec<GatekeeperEvent>,
    ) -> Self {
        TestGatekeeper {
            input,
            output: vec![],
            config,
            fsm,
        }
    }
}
