use std::boxed::FnBox;
use std::collections::hash_map::Entry::{Occupied, Vacant};
use std::collections::HashMap;
use std::marker::PhantomData;
use std::mem;
use std::mem::Discriminant;

/// Type that represents a transaction ID.
pub type RpcTrans = u32;

/// Error type for things that can go wrong with RPC things.
#[derive(Debug)]
pub enum RpcError {
    Timeout,
}

// Using a callback-style API for now...

/**
 *
 * This trait is implemented by FSMs that want to make requests and
 * act on responses to them.
 *
 */
pub trait Rpc<Request, Response> {
    /// Send a request.
    fn send(&mut self, req: Request, trans_id: RpcTrans);
}

pub type RpcCallback<Response, Container> =
    Box<FnBox(&mut Container, Result<Response, RpcError>) -> ()>;

pub struct RpcRecord<Request, Response, Container: Rpc<Request, Response>> {
    on_recv: RpcCallback<Response, Container>,
    discriminant: Discriminant<Response>,
    _dummy: PhantomData<Request>,
}

/**
 *
 * Held by FSMs to keep data about pending requests.
 *
 */
pub struct RpcManager<'a, Request, Response, Container: 'a + Rpc<Request, Response>> {
    pending: HashMap<RpcTrans, RpcRecord<Request, Response, Container>>,
    next_request_number: RpcTrans,
    container: &'a mut Container,
    dummy: PhantomData<Request>,
}

impl<'a, Request, Response, Container: Rpc<Request, Response>>
    RpcManager<'a, Request, Response, Container>
{
    /// Send a request and plan to execute the callback on the response.
    pub fn request(&mut self, req: Request, on_recv: RpcCallback<Response, Container>) {
        // Get the next request number
        let reqno = self.next_request_number;
        self.next_request_number += 1;
        let discriminant_req = mem::discriminant(&req);
        let discriminant_res = unsafe {
            mem::transmute::<Discriminant<Request>, Discriminant<Response>>(discriminant_req)
        };
        self.pending.insert(
            reqno,
            RpcRecord {
                on_recv,
                discriminant: discriminant_res,
                _dummy: PhantomData,
            },
        );
        self.container.send(req, reqno);
    }
    /// Acknowledge an incoming response so the callback for the request
    /// can be executed.
    /// Does nothing if no entry with the appropriate ID is present.
    pub fn receive_response(&mut self, res: Response, res_id: RpcTrans) {
        match self.pending.entry(res_id) {
            Vacant(_) => return, // Ignore non-pending entries
            Occupied(o) => {
                // Do the discriminants match?
                if o.get().discriminant != mem::discriminant(&res) {
                    return;
                }
                let rec = o.remove();
                rec.on_recv.call_box((self.container, Ok(res)));
            }
        }
    }
}
