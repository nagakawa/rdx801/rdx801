fn return_true() -> bool {
    true
}

#[derive(Serialize, Deserialize, Debug)]
pub struct School {
    pub name: String,
    pub users_called: String,
    // Values that affect base health.
    // The following is based on the following table:
    // http://www.wizard101central.com/wiki/Basic:Level_Chart#Health.2C_Mana.2C_Power_Pip_Chance.2C_Energy
    // An important difference: Wizard101 starts at level 1,
    // but Experiment801 starts at level 0. We extrapolate to level 0.
    // Also, Wizard101 doesn't have linear base health increases
    // per level. We simplify here.
    pub health_at_level_0: u32,
    pub health_per_level: u32,
    #[serde(default = "return_true")]
    pub is_usable: bool,
    // TODO how should masteries be specified?
}

impl School {
    pub fn get_base_health(&self, level: u32) -> u32 {
        self.health_at_level_0 + level * self.health_per_level
    }
}
