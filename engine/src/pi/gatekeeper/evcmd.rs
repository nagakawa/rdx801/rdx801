use pi::packet::*;
use pi::rpc::RpcTrans;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum GatekeeperEventType {
    FromClient(RpcTrans, CgRequest),
    FromShardReq(ShardId, RpcTrans, SgRequest),
    FromShardRes(ShardId, RpcTrans, GsResponse),
    FromShardP(ShardId, SgPacket),
    FromDb(RpcTrans, XdResponse),
    AddShard(ShardId),
    RemoveShard(ShardId),
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct GatekeeperEvent {
    pub timestamp: Timestamp,
    pub content: GatekeeperEventType,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum GatekeeperCommand {
    ToClient(RpcTrans, CgResponse),
    ToShardReq(ShardId, RpcTrans, GsRequest),
    ToShardRes(ShardId, RpcTrans, SgResponse),
    ToDb(RpcTrans, XdRequest),
}
