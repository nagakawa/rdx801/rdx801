pub mod abstract_gatekeeper;
pub mod evcmd;
mod fsm;

pub type Gatekeeper<'c, 'f, 's, E> = abstract_gatekeeper::Gatekeeper<'c, 'f, 's, E>;
pub type GatekeeperFsm<'a, 's> = fsm::GatekeeperFsm<'a, 's>;
pub type GatekeeperEvent = evcmd::GatekeeperEvent;
