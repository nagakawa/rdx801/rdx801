use std::time::Instant;

use pi::packet::*;
use pi::rpc::RpcTrans;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum DbEventType {
    FromShard(ShardId, RpcTrans, XdRequest),
    FromGatekeeper(RpcTrans, XdRequest),
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct DbEvent {
    pub timestamp: Timestamp,
    pub content: DbEventType,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum DbCommand {
    ToShard(ShardId, RpcTrans, XdResponse),
    ToGatekeeper(RpcTrans, XdResponse),
}
