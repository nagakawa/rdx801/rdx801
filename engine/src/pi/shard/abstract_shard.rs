use pi::configs::ShardConfig;
use pi::shard::evcmd::{ShardCommand, ShardEventType};
use pi::shard::{ShardEvent, ShardFsm};

pub trait Shard<'c, 'f, 's: 'c, E> {
    fn new(config: &'c ShardConfig<'s>, fsm: &'f mut ShardFsm<'c, 's>) -> Self;
    fn get_fsm(&mut self) -> &mut ShardFsm<'c, 's>;
    fn recv_response(&mut self, cmd: ShardCommand) -> ();
    fn start(&mut self) -> Result<(), E>;
    fn send_event(&mut self, ev: ShardEvent) -> () {
        self.get_fsm().recv(ev);
        let mut v: Vec<_> = self.get_fsm().outgoing.drain(..).collect();
        for cmd in v.drain(..) {
            self.recv_response(cmd);
        }
    }
}
