use pi::client::abstract_client::Client;
use pi::client::evcmd::{ClientCommand, ClientEventType};
use pi::client::{ClientEvent, ClientFsm};
use pi::configs::ClientConfig;

pub struct TestClient<'c, 'f> {
    input: Vec<ClientEvent>,
    pub output: Vec<ClientCommand>,
    config: &'c ClientConfig,
    fsm: &'f mut ClientFsm<'c>,
}

impl<'c, 'f> Client<'c, 'f, ()> for TestClient<'c, 'f> {
    fn new(config: &'c ClientConfig, fsm: &'f mut ClientFsm<'c>) -> Self {
        TestClient {
            input: vec![],
            output: vec![],
            config,
            fsm,
        }
    }
    fn get_fsm(&mut self) -> &mut ClientFsm<'c> {
        self.fsm
    }
    fn recv_response(&mut self, cmd: ClientCommand) {
        self.output.push(cmd);
    }
    fn start(&mut self) -> Result<(), ()> {
        // If anyone knows of a better way than this to appease the
        // compiler, then let me know.
        let mut v: Vec<_> = self.input.drain(..).collect();
        for ev in v.drain(..) {
            self.send_event(ev);
        }
        Ok(())
    }
}

impl<'c, 'f> TestClient<'c, 'f> {
    pub fn with_input(
        config: &'c ClientConfig,
        fsm: &'f mut ClientFsm<'c>,
        input: Vec<ClientEvent>,
    ) -> Self {
        TestClient {
            input,
            output: vec![],
            config,
            fsm,
        }
    }
}
