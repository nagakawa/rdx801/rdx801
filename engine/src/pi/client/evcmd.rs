use pi::packet::*;
use pi::rpc::RpcTrans;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum ClientEventType {
    FromGatekeeper(RpcTrans, CgResponse),
    FromShard(RpcTrans, CsResponse),
    FromShardP(ScPacket),
    File(FileResponse),
    Tick,
    // KeyInput(u64),
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct ClientEvent {
    pub timestamp: Timestamp,
    pub content: ClientEventType,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub enum ClientCommand {
    ToGatekeeper(RpcTrans, CgRequest),
    ToShard(RpcTrans, CsRequest),
    ToShardP(CsPacket),
    File(FileRequest),
    // Render(ClientRenderCommand),
}
