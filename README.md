## Experiment801

[*Join our discord!*](https://discord.gg/sDbNH5N)

> Thousands of candles can be lit from a single candle, and the life of the
> candle will not be shortened. Happiness never decreases by being shared.
> ~ Wizard101

A customisable MMORPG with Wizard101-like mechanics that anyone can modify or
run a server for.

This is an experimental codebase, different from the old C++ codebase.

### Building instructions

First, make sure you have Rust and Cargo installed.

Go into the `engine/` directory and type `cargo build`.

### Licence

Copyright (C) 2018 AGC.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
