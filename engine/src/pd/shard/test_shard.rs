use pi::configs::ShardConfig;
use pi::shard::abstract_shard::Shard;
use pi::shard::evcmd::{ShardCommand, ShardEventType};
use pi::shard::{ShardEvent, ShardFsm};

pub struct TestShard<'c, 'f, 's: 'c> {
    input: Vec<ShardEvent>,
    pub output: Vec<ShardCommand>,
    config: &'c ShardConfig<'s>,
    fsm: &'f mut ShardFsm<'c, 's>,
}

impl<'c, 'f, 's> Shard<'c, 'f, 's, ()> for TestShard<'c, 'f, 's> {
    fn new(config: &'c ShardConfig<'s>, fsm: &'f mut ShardFsm<'c, 's>) -> Self {
        TestShard {
            input: vec![],
            output: vec![],
            config,
            fsm,
        }
    }
    fn get_fsm(&mut self) -> &mut ShardFsm<'c, 's> {
        self.fsm
    }
    fn recv_response(&mut self, cmd: ShardCommand) {
        self.output.push(cmd);
    }
    fn start(&mut self) -> Result<(), ()> {
        // If anyone knows of a better way than this to appease the
        // compiler, then let me know.
        let mut v: Vec<_> = self.input.drain(..).collect();
        for ev in v.drain(..) {
            self.send_event(ev);
        }
        Ok(())
    }
}

impl<'c, 'f, 's> TestShard<'c, 'f, 's> {
    pub fn with_input(
        config: &'c ShardConfig<'s>,
        fsm: &'f mut ShardFsm<'c, 's>,
        input: Vec<ShardEvent>,
    ) -> Self {
        TestShard {
            input,
            output: vec![],
            config,
            fsm,
        }
    }
}
