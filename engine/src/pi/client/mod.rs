pub mod abstract_client;
pub mod evcmd;
mod fsm;

pub type Client<'c, 'f, E> = abstract_client::Client<'c, 'f, E>;
pub type ClientFsm<'a> = fsm::ClientFsm<'a>;
pub type ClientEvent = evcmd::ClientEvent;
