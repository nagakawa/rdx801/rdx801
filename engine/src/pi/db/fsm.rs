use std::collections::VecDeque;

use pi::configs::DbConfig;
use pi::db::evcmd::{DbCommand, DbEvent};

pub struct DatabaseFsm<'a, 's: 'a> {
    pub outgoing: VecDeque<DbCommand>,
    config: &'a DbConfig<'s>,
}

impl<'a, 's> DatabaseFsm<'a, 's> {
    pub fn new(config: &'a DbConfig<'s>) -> Self {
        DatabaseFsm {
            outgoing: VecDeque::new(),
            config,
        }
    }
    pub fn recv(&mut self, ev: DbEvent) -> () {
        // ???
    }
    pub fn send_cmd(&mut self, cmd: DbCommand) -> () {
        self.outgoing.push_back(cmd);
    }
}
