#![feature(arbitrary_self_types)]
#![feature(fnbox)]
#![feature(futures_api)]
#![feature(generators, generator_trait)]
#![feature(pin)]

#[macro_use]
extern crate rdx801_engine_macros;
#[macro_use]
extern crate arrayref;
extern crate num_traits;
#[macro_use]
extern crate serde_derive;
extern crate serde;

pub mod pd;
pub mod pi;

#[cfg(test)]
mod tests {
    use pd::gatekeeper::TestGatekeeper;
    use pi::configs::{GatekeeperConfig, ServerLayout};
    use pi::gatekeeper::abstract_gatekeeper::Gatekeeper;
    use pi::gatekeeper::evcmd::{GatekeeperCommand, GatekeeperEventType};
    use pi::gatekeeper::*;
    use pi::packet::{CgRequest, CgResponse, Timestamp};
    use std::net::SocketAddr;
    use std::str::FromStr;
    #[test]
    fn test_school_base_health() {
        use pi::combat::School;
        let fire = School {
            name: String::from("Fire"),
            users_called: String::from("Pyromancer"),
            health_at_level_0: 403,
            health_per_level: 22,
            is_usable: true,
        };
        assert_eq!(fire.get_base_health(0), 403);
        assert_eq!(fire.get_base_health(1), 425);
        assert_eq!(fire.get_base_health(10), 623);
        assert_eq!(fire.get_base_health(100), 2603);
    }
    static OKITA: &str =
        "vast kai e liiz okita! ya, main noan et passo. non ke sen halmel tisse! oho!?";
    fn get_test_layout() -> ServerLayout {
        let layout = ServerLayout {
            gatekeeper: SocketAddr::from_str("127.0.0.1:9004").unwrap(),
            database: SocketAddr::from_str("127.0.0.1:9005").unwrap(),
            shards: None,
        };
        layout
    }
    fn get_test_gk_config<'a>(layout: &'a ServerLayout) -> GatekeeperConfig<'a> {
        let config = GatekeeperConfig {
            server_layout: &layout,
            motd: String::from(OKITA),
            filehost_uri: String::from(":9006"),
        };
        config
    }
    #[test]
    fn test_gatekeeper_handles_motd_request() {
        let layout = get_test_layout();
        let config = get_test_gk_config(&layout);
        let mut fsm = GatekeeperFsm::new(&config);
        let mut gk = TestGatekeeper::with_input(
            &config,
            &mut fsm,
            vec![GatekeeperEvent {
                timestamp: Timestamp::now(),
                content: GatekeeperEventType::FromClient(0, CgRequest::Motd),
            }],
        );
        gk.start().unwrap();
        assert_eq!(
            gk.output,
            vec![GatekeeperCommand::ToClient(
                0,
                CgResponse::Motd(String::from(OKITA))
            ),]
        );
    }
    #[test]
    fn test_gatekeeper_handles_fhost_name_request() {
        let layout = get_test_layout();
        let config = get_test_gk_config(&layout);
        let mut fsm = GatekeeperFsm::new(&config);
        let mut gk = TestGatekeeper::with_input(
            &config,
            &mut fsm,
            vec![GatekeeperEvent {
                timestamp: Timestamp::now(),
                content: GatekeeperEventType::FromClient(0, CgRequest::Filehost),
            }],
        );
        gk.start().unwrap();
        assert_eq!(
            gk.output,
            vec![GatekeeperCommand::ToClient(
                0,
                CgResponse::Filehost(String::from(":9006"))
            ),]
        );
    }
    #[test]
    fn test_gatekeeper_can_add_and_remove_shards() {
        let layout = get_test_layout();
        let config = get_test_gk_config(&layout);
        let mut fsm = GatekeeperFsm::new(&config);
        {
            let mut gk = TestGatekeeper::with_input(
                &config,
                &mut fsm,
                vec![GatekeeperEvent {
                    timestamp: Timestamp::now(),
                    content: GatekeeperEventType::AddShard(44),
                },
                GatekeeperEvent {
                    timestamp: Timestamp::now(),
                    content: GatekeeperEventType::AddShard(44),
                },
                GatekeeperEvent {
                    timestamp: Timestamp::now(),
                    content: GatekeeperEventType::AddShard(69),
                },
                GatekeeperEvent {
                    timestamp: Timestamp::now(),
                    content: GatekeeperEventType::RemoveShard(44),
                },],
            );
            gk.start().unwrap();
        }
        assert!(fsm.has_shard(69));
        assert!(!fsm.has_shard(44));
    }
}
